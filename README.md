# TRABAJANDO CON GIT
***
### 1. Creamos el repositorio en remoto en GitLab

Crea un repositorio en GitLab o GitHub llamado TUNOMBRE_markdown 

![Imagen 1][1] 
![Imagen 2][2]
![Imagen 3][3]
![Imagen 4][4]

***
### 2. Creamos el repositorio en remoto en GitLab

![Imagen 5][5]
![Imagen 6][6]
![Imagen 7][7]

***
### 3.	Creación del archivo README.md

Crea en tu repositorio local un documento README.md Nota: en este documento tendrás que ir poniendo los comandos que has tenido que utilizar durante todos los ejercicios y las explicaciones y capturas de pantalla que consideres necesarias 

![Imagen 8][8]

***
### 4. Primer commit

Añadir al README.md los comandos utilizados hasta ahora y hacer un commit inicial con el mensaje “Primer commit de TUNOMBRE”. 

Nos situamos en la carpeta del repositorio y con ***git status*** comprobamos los archivos que están modificados.

![Imagen 9][9]

Ahora los añadimos con ***git add .*** y comprobamos que están listos para subir

![Imagen 10][10]

Ahora hacemos un commit con ***git commit -m "Comentario del commit"***

![Imagen 11][11]

***
### 5.	Subida de cambios al repositorio remoto

Sube los cambios al repositorio remoto. 

![Imagen 12][12]

***
### 6. Creamos ficheros en el repositorio local

Crear en el repositorio local un fichero llamado privado.txt. Crear en el repositorio local una carpeta llamada privada. 

![Imagen 13][13]

***
### 7. Ignoramos los cambios

Realizar los cambios oportunos para que tanto el archivo como la carpeta sean ignorados por git. 

Miramos que tenemos archivos modificados 

![Imagen 14][14]

Ahora creamos el archivo .gitignore con ***nano .gitignore*** en el cual meteremos los datos de los archivos que queremos que nos ignore

![Imagen 15][15]

Observamos que los archivos que tienen **"privad"** son ignorados al hacer el ***git status***

![Imagen 16][16]

***
### 8. Seguir documentando

Documenta los puntos 5., 6. y 7. en el fichero README.md 

***
### 9. Creación del fichero tunombre.md

Añade el fichero tunombre.md en el que se muestre un listado de los módulos en los que estás matriculado. 

![Imagen 17][17]

***
### 10. Crear un tag

Crea un tag llamado v0.1 

![Imagen 18][18]

***
### 11. Subir cambios al repositorio remoto

Sube los cambios al repositorio remoto

![Imagen 19][19]

***
### 12. Repositorios de compañeros

Por último, crea una tabla en el documento anterior en el que se muestre el nombre de 2 compañeros y su enlace al repositorio en GitLab o GitHub.

Nombre | Enlace al repositorio
-- | --
Juan Carlos R. | [Repositorio de Juan Carlos Rios](https://gitlab.com/juancrafff/juancarlos_makdown "título")
Joaquin B. | [Repositorio de Joaquín Blanco](https://gitlab.com/JoaKk/joaquin_markdown "título")


***
## Continuación --> Hoja03_Markdown_03

***
### Creación de ramas

Crea la rama rama-TUNOMBRE

![Imagen 20][20]

Posiciona tu carpeta de trabajo en esta rama


![Imagen 21][21]

***
### Añade un fichero y crea la rama remota

Crea un fichero llamado despligue.md con únicamente una cabecera DESPLIEGUE DE
APLICACIONES WEB (***nano despliegue.md***)

![Imagen 22][22]

Haz un commit con el mensaje “Añadiendo el archivo despliegue.md en la ramaTUNOMBRE”

Sube los cambios al repositorio remoto. NOTA: date cuenta que ahora se deberá hacer
con el comando git push origin rama-TUNOMBRE

![Imagen 23][23]

***
### Haz un merge directo

Posiciónate en la rama master

Haz un merge de la rama-TUNOMBRE en la rama master

![Imagen 24][24]

***
### Haz un merge con conflicto
En la rama master añade al fichero despliegue.md una tabla en la que muestres los
temas de la primera evaluación de Despliegue de Aplicaciones Web

![Imagen 25][25]

Añade los archivos y haz un commit con el mensaje “Añadida primera evaluación
Despliegue”

![Imagen 26][26]

Posiciónate ahora en la rama-TUNOMBRE

![Imagen 27][27]

Escribe en el fichero despliegue.md otra tabla con los temas de la segunda evaluación
de Despliegue de Aplicaciones Web

![Imagen 28][28]

Añade los archivos y haz un commit con el mensaje “Añadida tabla segunda evaluación
Despliegue”

![Imagen 29][29]

Posiciónate otra vez en master y haz un merge con la rama-TUNOMBRE

![Imagen 30][30]

***
### Arreglo del conflicto

Arregla el conflicto editando el fichero despliegue.md y haz un commit con el mensaje
“Finalizado el conflicto de despliegue.md”

![Imagen 31][31]

![Imagen 32][32]

***
### Tag y borrar la rama
Crea un tag llamado v0.2

Borra la rama-TUNOMBRE

![Imagen 33][33]

![Imagen 34][34]

***
### Finalizamos el trabajo

![Imagen 35][35]




<span style="color:red">Hola</span>


[1]: capturas/1.PNG
[2]: capturas/2.PNG
[3]: capturas/3.PNG
[4]: capturas/4.PNG
[5]: capturas/5.PNG
[6]: capturas/6.PNG
[7]: capturas/7.PNG
[8]: capturas/8.PNG
[9]: capturas/9.PNG
[10]: capturas/10.PNG
[11]: capturas/11.PNG
[12]: capturas/12.PNG
[13]: capturas/13.PNG
[14]: capturas/14.PNG
[15]: capturas/15.PNG
[16]: capturas/16.PNG
[17]: capturas/17.PNG
[18]: capturas/18.PNG
[19]: capturas/19.PNG
[20]: capturas/20.PNG
[21]: capturas/21.PNG
[22]: capturas/22.PNG
[23]: capturas/23.PNG
[24]: capturas/24.PNG
[25]: capturas/25.PNG
[26]: capturas/26.PNG
[27]: capturas/27.PNG
[28]: capturas/28.PNG
[29]: capturas/29.PNG
[30]: capturas/30.PNG
[31]: capturas/31.PNG
[32]: capturas/32.PNG
[33]: capturas/33.PNG
[34]: capturas/34.PNG
[35]: capturas/35.PNG
[36]: capturas/36.PNG